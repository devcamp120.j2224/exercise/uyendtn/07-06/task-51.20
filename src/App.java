import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList <Integer> mArrayList = new ArrayList<Integer>();
        mArrayList.add(1);
        mArrayList.add(2);
        mArrayList.add(3);
        mArrayList.add(4);
        mArrayList.add(5);
        mArrayList.add(6);

        for (int i = 0; i < mArrayList.size(); i++){
            //System.out.println(mArrayList.get(i));
        }
        //cách 1
        //App.convertArrListToArr(mArrayList);
        //cách 2
       // App.convertArrListToArr2(mArrayList);
        //cách 3
        App.convertArrListToArr3(mArrayList);
    }
    //cách 1: dùng stream().toArray(size -> new Integer[size])
    public static void convertArrListToArr(ArrayList<Integer> paramArrList){
        Integer[] myArray = paramArrList.stream().toArray(size -> new Integer[size]);
        for (Integer i:myArray){
            System.out.println(i + ",");
        }

        System.out.println(myArray.length);
    }
    //cách 2: dùng stream().toArray(Integer::new)
    public static void convertArrListToArr2(ArrayList<Integer> paramArrList){
        Integer[] myArray = paramArrList.stream().toArray(Integer[]::new);
        for (Integer i:myArray){
            System.out.println(i + ",");
        }

        System.out.println(myArray.length);
    }
    //cách 3 dùng mapToInt
    public static void convertArrListToArr3(ArrayList<Integer> paramArrList){
        int[] myArray = paramArrList.stream().mapToInt(i->i).toArray();
        for (int i:myArray){
            System.out.println(i + ",");
        }

        System.out.println(myArray.length);
    }
}
