import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class ExampleArray {
    /**
     * @param args
     */
    public static void main(String[] args) {
        final Random random = new Random();
        final Integer[] myArray = new Integer[5];

        for (int i = 0; i < myArray.length; i++){
            myArray[i] = Integer.valueOf(random.nextInt());
            System.out.println(myArray[i]);
        }
        //cách 1 dùng aslist
       // ExampleArray.convertArrToArrList(myArray);
        //cách 2 dùng collection
       // ExampleArray.convertArrToArrList2(myArray);
        //cách 3 dùng vòng lặp for
        ExampleArray.convertArrToArrList3(myArray);

    }
    //cách 1 dùng aslist
    public static void convertArrToArrList(Integer[] pArray) {
        ArrayList<Integer> mArrayList = new ArrayList<>(Arrays.asList(pArray));
        System.out.println(mArrayList);
    }
    //cách 2 dùng collection
    public static void convertArrToArrList2(Integer[] pArray) {
        ArrayList<Integer> mArrayList = new ArrayList<>();
        Collections.addAll(mArrayList, pArray);
        System.out.println(mArrayList);
    }
    //cách 3 dùng vòng lặp for
    public static void convertArrToArrList3(Integer[] pArray) {
        ArrayList<Integer> mArrayList = new ArrayList<>();
        for (Integer i:pArray){
            mArrayList.add(i);
        }
        System.out.println(mArrayList);
    }
    
}
